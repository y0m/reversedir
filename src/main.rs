#![feature(iter_intersperse)]
#![feature(test)]

extern crate test;

use std::borrow::Cow;

fn main() {
    println!("reversedir2: '{}'", reversedir2("toto;titi;tata"));
    println!("reversedir2: '{}'", reversedir2("toto;;tata"));
    println!("reversedir2: '{}'", reversedir2("toto;titi"));
    println!("reversedir2: '{}'", reversedir2("toto;"));
    println!("reversedir2: '{}'", reversedir2("toto"));
    println!("reversedir2: '{}'", reversedir2(""));

    println!();

    println!("reversedir3: '{}'", reversedir3("toto;titi;tata"));
    println!("reversedir3: '{}'", reversedir3("toto;;tata"));
    println!("reversedir3: '{}'", reversedir3("toto;titi"));
    println!("reversedir3: '{}'", reversedir3("toto;"));
    println!("reversedir3: '{}'", reversedir3("toto"));
    println!("reversedir3: '{}'", reversedir3(""));

    println!();

    println!("reversedir4: '{}'", reversedir4("toto;titi;tata"));
    println!("reversedir4: '{}'", reversedir4("toto;;tata"));
    println!("reversedir4: '{}'", reversedir4("toto;titi"));
    println!("reversedir4: '{}'", reversedir4("toto;"));
    println!("reversedir4: '{}'", reversedir4("toto"));
    println!("reversedir4: '{}'", reversedir4(""));
}

fn reversedir2<'a>(s: &'a str) -> Cow<'a, str> {
    if !s.contains(';') {
        s.into()
    } else {
        s.split(';')
            .take(2)
            .filter(|s| !s.is_empty())
            .collect::<Vec<_>>()
            .iter()
            .rfold(String::with_capacity(s.len()), |acc, el| {
                concat_dir2(acc, *el)
            })
            .into()
    }
}

fn concat_dir2(mut s: String, with: &str) -> String {
    if !s.is_empty() {
        s.push_str(" ");
    }
    s.push_str(with);
    s
}

fn reversedir3<'a>(s: &'a str) -> Cow<'a, str> {
    match *s
        .split(';')
        .take(2)
        .filter(|s| !s.is_empty())
        .collect::<Vec<_>>()
    {
        [first, second, ..] => format!("{} {}", second, first).into(),
        [first, ..] => first.into(),
        _ => s.into(),
    }
}

fn reversedir4<'a>(s: &'a str) -> Cow<'a, str> {
    s.split(';')
        .take(2)
        .filter(|s| !s.is_empty())
        .intersperse(&" ")
        .collect::<Vec<_>>()
        .into_iter()
        .rev()
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::{black_box, Bencher};

    #[bench]
    fn bench_reversedir2_all(b: &mut Bencher) {
        b.iter(|| black_box(reversedir2("toto;titi;tata")));
    }
    #[bench]
    fn bench_reversedir2_3_2(b: &mut Bencher) {
        b.iter(|| black_box(reversedir2("toto;;tata")));
    }
    #[bench]
    fn bench_reversedir2_2(b: &mut Bencher) {
        b.iter(|| black_box(reversedir2("toto;titi")));
    }
    #[bench]
    fn bench_reversedir2_1(b: &mut Bencher) {
        b.iter(|| black_box(reversedir2("toto")));
    }
    #[bench]
    fn bench_reversedir2_0(b: &mut Bencher) {
        b.iter(|| black_box(reversedir2("")));
    }

    #[bench]
    fn bench_reversedir3_all(b: &mut Bencher) {
        b.iter(|| black_box(reversedir3("toto;titi;tata")));
    }
    #[bench]
    fn bench_reversedir3_3_2(b: &mut Bencher) {
        b.iter(|| black_box(reversedir3("toto;;tata")));
    }
    #[bench]
    fn bench_reversedir3_2(b: &mut Bencher) {
        b.iter(|| black_box(reversedir3("toto;titi")));
    }
    #[bench]
    fn bench_reversedir3_1(b: &mut Bencher) {
        b.iter(|| black_box(reversedir3("toto")));
    }
    #[bench]
    fn bench_reversedir3_0(b: &mut Bencher) {
        b.iter(|| black_box(reversedir3("")));
    }

    #[bench]
    fn bench_reversedir4_all(b: &mut Bencher) {
        b.iter(|| black_box(reversedir4("toto;titi;tata")));
    }
    #[bench]
    fn bench_reversedir4_3_2(b: &mut Bencher) {
        b.iter(|| black_box(reversedir4("toto;;tata")));
    }
    #[bench]
    fn bench_reversedir4_2(b: &mut Bencher) {
        b.iter(|| black_box(reversedir4("toto;titi")));
    }
    #[bench]
    fn bench_reversedir4_1(b: &mut Bencher) {
        b.iter(|| black_box(reversedir4("toto")));
    }
    #[bench]
    fn bench_reversedir4_0(b: &mut Bencher) {
        b.iter(|| black_box(reversedir4("")));
    }
}
